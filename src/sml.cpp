/*
Copyright (C) 2016 Thomas Rostrup Andersen. All rights reserved.
License: BSD 2-clause
Version 0.1.0
*/
#include "sml.hpp"

State::State(std::string name, std::vector<std::string> initial_states):
    name(name),
    initial_states(initial_states),
    context(this)
{
    this->is_loop_stoped = true;
    this->are_events_handled = false;
    this->is_active = false;
}

/* Add states as substates to this state. */
void State::add_states(std::vector<State*> states){
    for (auto state : states){
        state->context = this;
        this->substates.push_back(state);
    }
}

/* This function is called when the state machine enter this state. Should be
overvritten by inheritance. */
void State::enter(){}

/* This function is called in loop as long as this state is active in the state
machine. It can return an event name. If the event leads to a state change,
the loop will end, if not, it will call execute again. Should be overvritten
by inheritance. WARNING: This function must be non-blocking. */
std::string State::execute(){return "";}

/* This function is called when the state machine leaves this state. Should be
 overvritten by inheritance. */
void State::leave(){}

/* Find all the inizial substates and start them. Start the execution_thread
for this state. */
void State::start(std::string event, std::string previous_state){
    for (auto name : this->initial_states){
        for (auto state : this->substates){
            if (state->name == name){
                this->current_states.push_back(state);
                state->start(event, previous_state);
            }
        }
    }
    this->event = event;
    this->previous_state = previous_state;
    this->thread = std::thread(&State::execution_thread, this);
    this->thread.detach();
}

/* The execution thread. First calls the enter() function, then loops over the
execution function until this->is_active == false. If execute() returns an
event, it calls evoke_event(). Calls the leave() function after loop is
terminated. */
void State::execution_thread(){
    this->enter();
    this->is_active = true;
    this->is_loop_stoped = false;
    while (this->is_active){ // Until the state is no longer active
        std::string event_name = this->execute();
        if (event_name != ""){
            this->evoke_event(event_name);
            while (not this->are_events_handled and this->is_active) {}
        }
    }
    this->leave();
    this->is_loop_stoped = true;
    this->cv_execution.notify_one();
}

/* Return the transistions that leads to the next states based on the event. */
std::vector<State::transition_t> State::find_transitions(std::string event_name){
    std::vector<transition_t> results = {};
    for (auto state : this->current_states){
        for (auto trans : this->transitions){
            if (trans.event == event_name and trans.previous_state == state->name){
                results.push_back(trans);
            }
        }
    }
    return results;
}

/* Stop this state and all it substates. Stopes the execution loop. */
void State::stop(){
    for (auto state : this->current_states){
        state->stop();
    }
    this->is_active = false;
    // Wait until notified by execution_thread that the loop is stoped.
    std::unique_lock<std::mutex> lk(this->execution_lock);
    this->cv_execution.wait_for(lk, std::chrono::milliseconds(100), [this]() -> bool{
        return this->is_loop_stoped;
    });
    lk.unlock();
}

/* Checks if any states that is hiarcly below this one can change state (based
on current state and the event) and changes the state. If multiple states in
the hiarchy can change state only the state furthest down will change state. It
is enough that one of paralell states changes. Return true if a state change
has occured.*/
bool State::trigger_event(std::string event_name){
    // Check if any active substate can handle the event.
    for (auto state : this->current_states){
        if (state->trigger_event(event_name)) {
            return true; // State did change futher down in the hiarchy
        }
    }
    // Find all transitions that should be executed and do the state change.
    std::vector<transition_t> transactions = find_transitions(event_name);
    for (auto trans : transactions){
        // Stop and remove current substates.
        for (auto state = this->current_states.begin(); state != this->current_states.end(); state++){
            if ((*state)->name == (trans.previous_state)){
                (*state)->stop();
                state = this->current_states.erase(state);
                if (state == this->current_states.end()){
                    state--;
                }
            }
        }
        // Start next substates and set as current.
        for (auto state : this->substates){
            if (state->name == (trans.next_state)){
                state->start(trans.event, trans.previous_state);
                this->current_states.push_back(state);
            }
        }
        return true;
    }
    return false;
}

/* Find the top state in the hiarchy and set it as the state machine for all
 states. Create a thread to handle events/triggers. Start the state machine. */
void State::start_state_machine(){
    this->state_machine = this->context;
    State *next_pointer = this->state_machine->context;
    while (next_pointer != this->state_machine){
        this->state_machine = next_pointer;
        next_pointer = this->state_machine->context;
    }
    this->state_machine->set_state_machine(this->state_machine);
    this->state_machine->is_active = true;
    this->state_machine->event_thread = std::thread(&State::event_handler, this->state_machine);
    this->state_machine->event_thread.detach();
    this->state_machine->start("start_state_machine", "Off");
}

/* Add event to the state machine's event queue and notify the event_handler. */
void State::evoke_event(std::string event_name){
    this->are_events_handled = false;
    event_t event(event_name, this);
    std::unique_lock<std::mutex> lk(this->state_machine->queue_lock);
    this->state_machine->event_queue.push(event);
    lk.unlock();
    this->state_machine->cv_queue.notify_one();
}

/* The event handler thread. Runs until state machine is stoped. Checks events
that has been added to the event_queue. Changes state if possible (bassed on
the event). Notifies the calling state that the event is handled. */
void State::event_handler(){
    while (this->state_machine->is_active){
        // Wait until notified by event_envoke about a new event in event queue
        // or until 1sec has passed (incase handler should be stoped).
        std::unique_lock<std::mutex> lk(this->state_machine->handler_lock);
        cv_queue.wait_for(lk, std::chrono::milliseconds(1000), [this]() -> bool{
            return !this->state_machine->event_queue.empty() or !this->state_machine->is_active;
        });
        lk.unlock();

        // Empty the queue or until stoped.
        while(!this->state_machine->event_queue.empty() and this->state_machine->is_active){
            std::unique_lock<std::mutex> lk(this->state_machine->queue_lock);
            event_t next_event = this->state_machine->event_queue.front();
            lk.unlock();
            this->trigger_event(next_event.name);
            // Notify the calling state that the event is handled.
            next_event.calling_state->are_events_handled = true;
            lk.lock();
            this->state_machine->event_queue.pop();
            lk.unlock();
        }
    }
}

/* Stope the state machine, and all it's threads. */
void State::stop_state_machine(){
    this->state_machine->is_active = false;
    this->stop();
}

/* Set state_machine as this->state_machine for all states hiarcly below this. */
void State::set_state_machine(State *&state_machine){
    for (auto state : this->substates){
        state->state_machine = this->state_machine;
        state->set_state_machine(state_machine);
    }
}

/* Return a vector of the active states hiarcly below this state. */
std::vector<State*> State::get_active_states(){
    std::vector<State*> active_states;
    for(auto state : this->current_states){
        active_states.push_back(state);
        std::vector<State*> l = state->get_active_states();
        active_states.insert(active_states.end(), l.begin(), l.end());
    }
    return active_states;
}

/* Return a vector of the active state names. */
std::vector<std::string> State::get_active_state_names(){
    std::vector<std::string> result;
    std::vector<State*> active_states = this->state_machine->get_active_states();
    for(auto state : active_states){
        result.push_back(state->name);
    }
    return result;
}
