/*
Copyright (C) 2016 Thomas Rostrup Andersen. All rights reserved.
License: BSD 2-clause
Version 0.1.0
*/
#ifndef sml_hpp
#define sml_hpp
#include <string>
#include <thread>
#include <mutex>
#include <queue>
#include <vector>
#include <condition_variable>
#include <atomic>

class State {
public:

    struct transition_t{
        std::string event;
        std::string previous_state;
        std::string next_state;
        transition_t(){}
        transition_t(std::string event, std::string previous_state,
            std::string next_state) :
                event(event),
                previous_state(previous_state),
                next_state(next_state)
        {}
    };

    // State API
    State(std::string name, std::vector<std::string> initial_states={});
    std::vector<transition_t> transitions;
    void add_states(std::vector<State*> states);

    // State machine API
    void start_state_machine();
    void stop_state_machine();
    void evoke_event(std::string event_name);
    std::vector<std::string> get_active_state_names();

protected:
    std::string name;

private:

    struct event_t{
        std::string name;
        State *calling_state = nullptr;
        event_t(){}
        event_t(std::string name, State *calling_state) :
            name(name),
            calling_state(calling_state)
        {}
    };

    // Virtual State functions to override.
    virtual void enter();
    virtual std::string execute();
    virtual void leave();

    // State
    void set_state_machine(State *&state_machine);
    void start(std::string event, std::string previous_state);
    void stop();
    void execution_thread();
    bool trigger_event(std::string event_name);
    std::vector<transition_t> find_transitions(std::string event_name);
    std::vector<State*> get_active_states();
    State *context = nullptr;
    State *state_machine = nullptr;
    std::vector<State*> substates;
    std::vector<State*> current_states;
    std::vector<std::string> initial_states;
    std::thread thread;
    std::atomic<bool> is_loop_stoped; // true;
    std::atomic<bool> are_events_handled; // false;
    std::string event;
    std::string previous_state;
    std::atomic<bool> is_active; // false;
    std::mutex execution_lock;
    std::condition_variable cv_execution;

    // State machine
    void event_handler();
    std::thread event_thread;
    std::queue<event_t> event_queue;
    std::mutex queue_lock;
    std::mutex handler_lock;
    std::condition_variable cv_queue;
};

#endif
