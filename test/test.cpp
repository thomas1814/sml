/*
Copyright (C) 2016 Thomas Rostrup Andersen. All rights reserved.
License: BSD 2-clause
Version 0.1.0

Compile:
g++ test.cpp catch.hpp ../src/sml.cpp -lpthread -o test.out -std=c++11

Run:
./test.out

*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/sml.hpp"

TEST_CASE( "Operational test of the state machine.", "[StateMachine]" ) {

    std::vector<std::string> initial_states = {"s1"};
    State state_machine("state_machine", initial_states);
    std::vector<std::string> initial_states2 = {"s21"};
    std::vector<std::string> initial_states3 = {"s31", "s32"};

    State state1("s1");
    State state2("s2", initial_states2);
    State state3("s3", initial_states3);
    State state4("s4");
    State state5("s5");
    State state21("s21");
    State state22("s22");
    State state23("s23");
    State state31("s31");
    State state32("s32");
    std::vector<State*> states;
    states.push_back(&state1);
    states.push_back(&state2);
    states.push_back(&state3);
    states.push_back(&state4);
    states.push_back(&state5);
    state_machine.add_states(states);
    std::vector<State*> statesfor2;
    statesfor2.push_back(&state21);
    statesfor2.push_back(&state22);
    statesfor2.push_back(&state23);
    state2.add_states(statesfor2);
    std::vector<State*> statesfor3;
    statesfor3.push_back(&state31);
    statesfor3.push_back(&state32);
    state3.add_states(statesfor3);

    State::transition_t t1("e1", "s1", "s1");
    State::transition_t t2("e1", "s4", "s1");
    State::transition_t t3("e1", "s3", "s1");
    State::transition_t t4("e2", "s1", "s2");
    State::transition_t t5("e3", "s2", "s3");
    State::transition_t t6("e3", "s5", "s3");
    State::transition_t t7("e5", "s4", "s5");
    State::transition_t t8("e4", "s1", "s4");
    State::transition_t t81("e1", "s2", "s1");

    State::transition_t t9("e1", "s21", "s22");
    State::transition_t t10("c3", "s21", "s23");
    State::transition_t t11("c1", "s23", "s21");
    State::transition_t t12("c2", "s23", "s22");
    State::transition_t t13("c2", "s22", "s22");
    State::transition_t t14("c3", "s22", "s23");

    state_machine.transitions = {t1, t2, t3, t4, t5, t6, t7, t8, t81};
    state2.transitions = {t9, t10, t11, t12, t13, t14};
    state_machine.start_state_machine();
    int wait_time = 1000;

    SECTION( "Test that transistions leads to correct new states." ) {
        /* The transitions in this test and expexted states:
        event   state
                s1
        e4      s4
        e1      s1
        e2      s2/s21
        e1      s2/s22
        c2      s2/s22
        c3      s2/s23
        e1      s1
        e4      s4
        e5      s5
        e3      s3/s31/s32
        e1      s1
        */
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        auto list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        for (auto it = list.begin(); it != list.end(); it++){
            REQUIRE( (*it) == "s1" );
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e4");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        for (auto it = list.begin(); it != list.end(); it++){
            REQUIRE( (*it) == "s4" );
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e1");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        for (auto it = list.begin(); it != list.end(); it++){
            REQUIRE( (*it) == "s1" );
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e2");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time*4));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 2 );
        auto it = list.begin();
        REQUIRE( (*it) == "s2" );
        it++;
        REQUIRE( (*it) == "s21" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e1");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 2 );
        it = list.begin();
        REQUIRE( (*it) == "s2" );
        it++;
        REQUIRE( (*it) == "s22" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("c2");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 2 );
        it = list.begin();
        REQUIRE( (*it) == "s2" );
        it++;
        REQUIRE( (*it) == "s22" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("c3");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 2 );
        it = list.begin();
        REQUIRE( (*it) == "s2" );
        it++;
        REQUIRE( (*it) == "s23" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e1");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        it = list.begin();
        REQUIRE( (*it) == "s1" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e4");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        it = list.begin();
        REQUIRE( (*it) == "s4" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e5");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        it = list.begin();
        REQUIRE( (*it) == "s5" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e3");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time*4));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 3 );
        it = list.begin();
        REQUIRE( (*it) == "s3" );
        it++;
        REQUIRE( (*it) == "s31" );
        it++;
        REQUIRE( (*it) == "s32" );
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

        state_machine.evoke_event("e1");
        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
        list = state_machine.get_active_state_names();
        REQUIRE( list.size() == 1 );
        it = list.begin();
        REQUIRE( (*it) == "s1" );

        std::this_thread::sleep_for(std::chrono::milliseconds(wait_time*2));
        state_machine.stop_state_machine();
    }
}
