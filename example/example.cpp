/*
Copyright (C) 2016 Thomas Rostrup Andersen. All rights reserved.
License: BSD 2-clause
Version 0.1.0

Compile:
g++ example.cpp ../src/sml.cpp -lpthread -o example.out -std=c++11

Run:
./example.out

This example shows a how to use the state machine libary (sml).
The implemented state machine used in this example can be viewed in the figure
named statediagram.png.
*/
#include "../src/sml.hpp"
#include <iostream>
#include <chrono>
#include <thread>

/* A CustomState that inherite from the state class. It implements the enter,
execute and leave member functions. */
class CustomState: public virtual State{
public:
    CustomState(std::string name, std::vector<std::string> initial_states =
        {} ) : State(name, initial_states){};

    void enter(std::string event_name, std::string previus_state){
        std::cout << "CustomState Enter " << this->name << std::endl;
    }

    std::string execute(){
        std::cout << "CustomState Execute " << this->name << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        std::cout << "CustomState says: " << this->data << std::endl;
        return "";
    }

    void leave(){
        std::cout << "CustomState Leave " << this->name << std::endl;
    }

    std::string data = "Hello, World!";
};


int main(int argc, char **arg){
    std::cout << "Start of Program." << std::endl;

    // Create a list of the inizial states of the state machine.
    std::vector<std::string> initial_states = {"s1"};

    // Create a state machine, give it a name and list of inizial states.
    // Recomanded to have unique names.
    // The state machine is technically just like any other state.
    State state_machine("state_machine", initial_states);

    // Create states that inherite from the CustomState.
    CustomState state1("s1");  // No initial_states since it has no substates
    CustomState state2("s2");

    // State 3 will have two substates that runs in paralle
    std::vector<std::string> initial_states3 = {"s31", "s32"};
    CustomState state3("s3", initial_states3);
    CustomState state31("s31");
    CustomState state32("s32");

    // Add state 1, 2 and 3 to the state machine
    std::vector<State*> states;
    states.push_back(&state1);
    states.push_back(&state2);
    states.push_back(&state3);
    state_machine.add_states(states);

    // Add substates 31 and 32 to state 3
    std::vector<State*> statesfor3;
    statesfor3.push_back(&state31);
    statesfor3.push_back(&state32);
    state3.add_states(statesfor3);

    // Create transitions for state 1, 2 and 3 and add them to the state
    // machine. Format is <event name, current state name, next state name>.
    State::transition_t t1("e1", "s2", "s1");
    State::transition_t t2("e1", "s3", "s1");
    State::transition_t t3("e2", "s1", "s2");
    State::transition_t t4("e3", "s1", "s3");
    State::transition_t t5("e3", "s2", "s3");
    state_machine.transitions = {t1, t2, t3, t4, t5};

    // Start the state machine
    state_machine.start_state_machine();
    int wait_time = 3000;
    std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

    // Evoke event e2 (and go to state 2)
    state_machine.evoke_event("e2");
    std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

    // Evoke event e3 (and go to state 3 - who has parallel states)
    // The printout may be messy since s3, s31 and s32 will print from 3
    // diffrent threads.if s3 itself does not need to do anything, it could
    // simply use the same state class as the state machine is using (then only
    // s31 and s32 would be printing).
    state_machine.evoke_event("e3");
    std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

    // Evoke event e1 (and go to state 1)
    state_machine.evoke_event("e1");
    std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));

    // Stop state machine
    state_machine.stop_state_machine();

    std::cout << "End of Program." << std::endl;
    return 0;
}
