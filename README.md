# README  
An open source state machine library (sml) written in C++11. The state machine supports state hierarchy (states with substates), states can run in parallel (concurrency) and events can be triggered from external sources and/or the states can return events themselves.  

## License  
License for sml: BSD 2-clause.  
sml is using Catch for testing (test folder). Catch is distributed under the Boost Software License, Version 1.0.  
NOTE: sml itself (in the src folder) can run without catch, so catch is only needed for running the test files.  

## How to use?  
Download source, include the sml.cpp and sml.hpp file in your project folder and include it in your source code:  
`` #include "sml.hpp"``  

A state machine contains states and the transitions between the states. A new state should inherit from the State class and implement the enter(), execute() and leave() functions:  
* enter(): This function is called when the state machine enters this state. Should be overwritten by inheritance.  
* execution(): This function is called in loop as long as this state is active in the state machine. It can return an event name. If the event leads to a state change, the loop will end, if not, it will call execute again. Should be overwritten by inheritance. WARNING: This function must be non-blocking.  
* leave(): This function is called when the state machine leaves this state. Should be overwritten by inheritance.  

The example included in the example folder should provide sufficient explanation on basic usage, however after looking at the example, a few more things can be worth pointing out:  

* The event that lead to the state change is stored in this->event.  
* The name of the previous state is stored in this->previous_state.  
* The top level state (machine) is stored in this->state_machine (after state machine is started).  
* The state hierarchy above this one is stored in this->context. If this is the (top state it is the same as this->state machine and this).  
* When evoke_event(event_name) is called, it will add the event to the event queue, the event_handler will process it, but that means that evoke_event() will return before the event_handler is finish with handling the event. A state's execution function can however return an event, and the execution will be halted until the event is handled by the event_handler. Returning "" will not halt the execution as it is ignored. If halting the execute is not desirable, this->state_machine.evoke_event() can be used instead from within the execution function.  
* Since starting the state machine is non-blocking, events can be triggered from outside of the state machine by state_machine.evoke_event(event_name).  

## Tests  
The tests is found in the test folder. The testing require the catch framework
(single header file catch.hpp). The test is using a test state machine. The state machine diagram is located in the test folder.  
